<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function bio(){
        return view('halaman.profile');
    }

    public function kirim(Request $request){
        $nama = $request['nama'];
        $alamat = $request['address'];
        return view('halaman.home', compact('nama', 'alamat'));
    }
}
